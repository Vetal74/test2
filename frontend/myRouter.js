/**
 * Created by Виталий on 06.07.2017.
 */
export default class MyRouter {
    constructor() {
        this.needSearch = false;
    }
    parseUrlToParams() {
        let url = location.hash.slice(1);
        let urls = url.split('&');
        let response = {
            name: '',
            new: '',
            price: ''
        };
        urls.forEach(item => {
            if(this.subStr(item, 'name')) {
                response.name = this.getParameter(item);
                this.searched();
            }
            if(this.subStr(item, 'new')) {
                response.new = this.getParameter(item);
                this.searched();
            }
            if(this.subStr(item, 'price')) {
                response.price = this.getParameter(item);
                this.searched();
            }
        });
        return response;
    }
    locationPush({name, isNew, price}) {
        history.pushState(null, null, `#name:${name}&price:${price}&new:${isNew}`);
    }
    searched() {
        this.needSearch = true;
    }
    subStr(str, substr) {
        return (str.indexOf(substr) + 1) > 0;
    }
    getParameter(item) {
        return item.split(':')[1];
    }
}