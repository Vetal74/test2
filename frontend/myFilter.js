/**
 * Created by Виталий on 05.07.2017.
 */
import _ from 'lodash';
export default class MyFilter {
    constructor() {}
    static search(query, data) {
        let responseData = data;
        if(query.price != '') {
            responseData = _.filter(responseData, object => {
                let intPrice = parseInt(query.price, 10);
                let objectIntPrice = MyFilter.getIntPrice(object.price);
                return objectIntPrice < intPrice;
            });
        }
        if(query.new != '') {
            responseData = _.filter(responseData, object => object.new === true);
        }
        if(query.name != '') {
            responseData = _.filter(responseData, object => {
                return (object.name.indexOf(query.name) + 1) > 0;
            });
        }
        return responseData;
    }
    static getIntPrice(price) {
        let intPrice = price.slice(1);
        return parseInt(intPrice, 10);
    }
}