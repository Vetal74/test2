/**
 * Created by Виталий on 05.07.2017.
 */
import Vue from 'vue';
import Vuex from 'vuex';
import MyRouter from './myRouter';
import StoreFilter from './components/filter.vue';
import jsonData from './MOCK_DATA.json';
import MyFilter from './myFilter';


let myRouter = new MyRouter();
let urlParams = myRouter.parseUrlToParams();
Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        results: jsonData
    },
    getters: {
        results(state) {
            return state.results;
        }
    },
    mutations: {
        setNames(state, {type, items}) {
            state[type] = items;
        }
    },
    actions: {
        search({commit}, query) {
            let results = MyFilter.search(query, jsonData);
            commit('setNames', {type: 'results', items: results});
        }
    }
});
let main = new Vue({
    el: '#app',
    components: {
        storefilter: StoreFilter
    },
    computed: {
        results() {
            return store.getters.results;
        }
    },
    data: {
        isNew: urlParams.new,
        nameProduct: urlParams.name,
        rangePrice: urlParams.price
    },
    methods: {
        search() {
            myRouter.locationPush({name: this.nameProduct, isNew: this.isNew, price: this.rangePrice});
            store.dispatch('search', {new: this.isNew, name: this.nameProduct, price: this.rangePrice});
        }
    }
});
if(myRouter.needSearch) {
    main.search();
}