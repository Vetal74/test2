/**
 * Created by Виталий on 05.07.2017.
 */
import gulp from 'gulp';
import browserify from 'browserify';
import watchify from 'watchify';
import babelify from 'babelify';
import vueify from 'vueify'
import less from 'gulp-less';
import rimraf from 'rimraf';
import source from 'vinyl-source-stream';
import browserSync from 'browser-sync';
let reload = browserSync.reload;

const config = {
    entryFile: './frontend/index.js',
    outputDir: './dist/',
    outputFile: 'index.js'
};

// clean the output directory
gulp.task('clean', function(cb){
    rimraf(config.outputDir, cb);
});

let bundler;
function getBundler() {
    if (!bundler) {
        bundler = watchify(browserify(config.entryFile, Object.assign({ debug: true }, watchify.args)));
    }
    return bundler;
}

function bundle() {
    return getBundler()
        .transform(vueify)
        .transform(babelify)
        .bundle()
        .on('error', function(err) { console.log('Error: ' + err.message); })
        .pipe(source(config.outputFile))
        .pipe(gulp.dest(config.outputDir))
        .pipe(reload({ stream: true }));
}

gulp.task('build-persistent', ['clean'], function() {
    return bundle();
});
gulp.task('build', ['build-persistent'], function() {
    process.exit(0);
});
gulp.task('buildcss', function() {
    return gulp.src('./frontend/css/*.less')
        .pipe(less({}))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('watch', ['build-persistent', 'serve', 'buildcss'], function() {
    getBundler().on('update', function() {
        gulp.start('build-persistent');
    });
    gulp.watch('./frontend/css/*.less', ['buildcss', 'build-persistent']);
});

// WEB SERVER
gulp.task('serve', function () {
    browserSync({
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default', ['watch', 'buildcss'], function() {
    console.log('Подняли сервер, выслеживаем изменяшки');
});